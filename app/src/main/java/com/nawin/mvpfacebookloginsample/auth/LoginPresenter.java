package com.nawin.mvpfacebookloginsample.auth;

import android.content.Context;
import android.content.Intent;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.nawin.mvpfacebookloginsample.data.model.User;

/**
 * Created by nawin on 4/8/17.
 */

public class LoginPresenter implements LoginContract.Presenter {
    private final CallbackManager callbackManager;

    private LoginContract.View view;
    private Context context;

    public LoginPresenter(Context context, LoginContract.View view) {
        this.view = view;
        this.context = context;
        ;

        //Initialize for fb login
        FacebookSdk.sdkInitialize(context);
        this.callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, onFbLogin());
        view.setPresenter(this);
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {

    }

    @Override
    public FacebookCallback<LoginResult> onFbLogin() {

        return new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String facebookId = loginResult.getAccessToken().getUserId();
                loginRegisterCheck(facebookId, User.TYPE_SOCIAL_MEDIA_FACEBOOK);

            }

            @Override
            public void onCancel() {
                view.showFbLoginError("Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                view.showFbLoginError(error.toString());
            }
        };
    }

    private void loginRegisterCheck(String facebookId, String typeSocialMediaFacebook) {
        view.showFbLoginProgress();

    }

    @Override
    public void onFbLoginResult(int requestCode, int resultCode, Intent data) {
        this.callbackManager.onActivityResult(requestCode, resultCode, data);
    //bla bla bla
        view.showFbLoginSuccess();
    }
}
