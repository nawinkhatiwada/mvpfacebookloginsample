package com.nawin.mvpfacebookloginsample.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.nawin.mvpfacebookloginsample.R;
import com.nawin.mvpfacebookloginsample.utils.Alert;

import static com.nawin.mvpfacebookloginsample.utils.Alert.showProgress;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    private LoginContract.Presenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LoginPresenter(getApplicationContext(), this);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void showFbLoginProgress() {
        progressDialog = showProgress(getApplicationContext(),R.string.please_wait);
    }

    @Override
    public void showFbLoginError(String message) {
        Log.d("Facebook Error----", message);
    }

    @Override
    public void showFbLoginSuccess() {
        // Do something
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            if (resultCode == LoginActivity.RESULT_OK)
                presenter.onFbLoginResult(requestCode, resultCode, data);
            else {
                //DO something
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
