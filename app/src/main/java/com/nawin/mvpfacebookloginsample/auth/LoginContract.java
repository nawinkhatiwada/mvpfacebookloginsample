package com.nawin.mvpfacebookloginsample.auth;

import android.content.Intent;

import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.nawin.mvpfacebookloginsample.BasePresenter;
import com.nawin.mvpfacebookloginsample.BaseView;

/**
 * Created by nawin on 4/8/17.
 */

public class LoginContract {

    interface Presenter extends BasePresenter{
        FacebookCallback<LoginResult> onFbLogin();
        void onFbLoginResult(int requestCode, int resultCode, Intent data);

    }

    interface View extends BaseView<Presenter>{
        void showFbLoginProgress();
        void showFbLoginError(String message);
        void showFbLoginSuccess();

    }
}
