package com.nawin.mvpfacebookloginsample.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by nawin on 4/8/17.
 */

public class Alert {

    public static ProgressDialog showProgress(Context context, @StringRes int message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(String.valueOf(message));
        progressDialog.show();
        return progressDialog;
    }
}
