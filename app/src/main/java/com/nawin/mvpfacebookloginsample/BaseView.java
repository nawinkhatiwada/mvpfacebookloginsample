
package com.nawin.mvpfacebookloginsample;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
