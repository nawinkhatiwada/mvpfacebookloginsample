
package com.nawin.mvpfacebookloginsample;

public interface BasePresenter {

    void start();

    void stop();

}
